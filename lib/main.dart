import 'package:flutter/material.dart';
import 'package:quizzler/Questions.dart';
import 'Questions.dart';

void main() => runApp(Quizzler());


class Quizzler extends StatelessWidget {

  @override

  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(

        backgroundColor: Colors.grey.shade900,

        body: SafeArea(

          child: Padding(

            padding: EdgeInsets.symmetric(horizontal: 10.0),

            child: QuizPage(),

          ),

        ),

      ),

    );

  }

}

class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {

   List <Icon> scoreKeeper = [];

  List <Questions> questionBank = [

    Questions(q: 'You can lead a cow down stairs but not up stairs.', a: false),
  Questions(q:  'Approximately one quarter of human bones are in the feet.', a: true),
  Questions(q: 'A slug\'s blood is green.', a: true ),

  ];


  int questionNumber = 0;


  @override

  Widget build(BuildContext context) {

    return Column(

      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

      crossAxisAlignment: CrossAxisAlignment.stretch,

      children: [

        Expanded (

          flex: 5,

          child: Padding(

            padding: const EdgeInsets.all(10.0),

            child: Center (

              child: Text (

                questionBank [questionNumber].questionText,

                textAlign: TextAlign.center,

                style: TextStyle(

                  fontSize: 20.0,

                  color: Colors.white,

                ),

              ),

            ),

          ),

        ),

        Expanded(

          child: Padding(

            padding: EdgeInsets.all(10.0),

            child: FlatButton(

              color: Colors.green,

              child: Text(

                  'TRUE',

                  style: TextStyle(

                    fontSize: 20.0,

                    color: Colors.white,

                  ),

              ),

              onPressed: () {

                //the user has picked true

                bool Correctanswer = questionBank[questionNumber].questionAnswer;

                if (Correctanswer == true) {

                  print ('user got it right ');

                } else {

                  print ('user got it wrong ');

                }

                setState(() {

                  questionNumber++ ;

                });

                print (questionNumber);

              },

            ),

          ),

        ),

        Expanded(

          child: Padding(

            padding: EdgeInsets.all(10.0),

            child: FlatButton(

              color: Colors.red,

              child: Text(

                  'FALSE',

                style: TextStyle(

                  fontSize: 20.0,

                  color: Colors.white,

                ),

                  ),

              onPressed: () {

                //the user has picked false

                bool Correctanswer = questionBank[questionNumber].questionAnswer;

                if (Correctanswer == false) {

                  print ('user got it right ');

                } else {

                  print ('user got it wrong ');

                }

                setState(() {

                  questionNumber++ ;

                });

                print (questionNumber);

              },

            ),

          ),

        ),

        Row (

          children: scoreKeeper,

        ),

      ],

    );

    }

  }



/*
question1: 'You can lead a cow down stairs but not up stairs.', false,
question2: 'Approximately one quarter of human bones are in the feet.', true,
question3: 'A slug\'s blood is green.', true,
*/
